# Weird

minimum viable code about a weird behaviour i notice with GenServer.

my issue is the following:
when an `handle_continue/2` takes more than a certain time (around `5_000`), the message is never sent back to the caller.

following can be done to witness behaviour:

``` elixir
# Working as expected:
iex(1)> Weird.Server.queue 42
# XX:XX:XX.XXX [info] appending element to queue, now: 1
:ok
# XX:XX:XX.XXX [info] consuming an element from the queue, now 0 elements remaining
# XX:XX:XX.XXX [info] received a report: quick

# ---

# Faulty behaviour:
# comment   lib/weird/server.ex:36
# uncomment lib/weird/server.ex:39

iex(1)> Weird.Server.queue 42
# XX:XX:XX.XXX [info] appending element to queue, now: 1
:ok
# XX:XX:XX.XXX [info] consuming an element from the queue, now 0 elements remaining
# absolutely no caught message from the client :(
iex(2)> 
```