defmodule Weird do
  @moduledoc """
  Documentation for `Weird`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Weird.hello()
      :world

  """
  def hello do
    :world
  end
end
