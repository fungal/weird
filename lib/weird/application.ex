defmodule Weird.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Weird.Server,
      Weird.Client
    ]

    opts = [strategy: :one_for_one, name: Weird.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
