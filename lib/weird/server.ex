defmodule Weird.Server do
  require Logger
  use GenServer

  @impl true
  def handle_cast({:queue, elem}, state) do
    new_state = state ++ [elem]
    Logger.info("appending element to queue, now: #{Enum.count(new_state)}")

    {:noreply, new_state}
  end

  @impl true
  def handle_info({_from, report}, state) do
    Logger.info("received a report: #{report}")

    schedule_work(0)
    {:noreply, state}
  end

  @impl true
  def handle_info(:consume_queue, [] = state) do
    Logger.debug("no element to consume in queue, rescheduling")
    schedule_work()
    {:noreply, state}
  end

  @impl true
  def handle_info(:consume_queue, state) do
    [_head | new_state] = state

    "consuming an element from the queue, now #{Enum.count(new_state)} elements remaining"
    |> Logger.info()

    # this will be successful
    GenServer.call(Weird.Client, :quick)

    # this will never receive a message back
    # GenServer.call(Weird.Client, :slow)

    {:noreply, new_state}
  end

  def handle_info(msg, state) do
    IO.inspect(msg)

    {:noreply, state}
  end

  def queue(elem) do
    GenServer.cast(__MODULE__, {:queue, elem})
  end

  defp schedule_work(backoff \\ 5) do
    Process.send_after(self(), :consume_queue, :timer.seconds(backoff))
  end

  def start_link(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  @impl true
  def init(_args) do
    schedule_work()
    {:ok, []}
  end
end
