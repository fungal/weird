defmodule Weird.Client do
  use GenServer

  @impl true
  def handle_call(:quick, from, _state) do
    {:reply, :ok, nil, {:continue, {:quick, from}}}
  end

  @impl true
  def handle_call(:slow, from, _state) do
    {:reply, :ok, nil, {:continue, {:slow, from}}}
  end

  @impl true
  def handle_continue({:quick, from}, _state) do
    GenServer.reply(from, :quick)

    {:noreply, nil}
  end

  @impl true
  def handle_continue({:slow, from}, _state) do
    :timer.sleep(6000)

    GenServer.reply(from, :slow)

    {:noreply, nil}
  end

  def start_link(default) do
    GenServer.start_link(__MODULE__, default, name: __MODULE__)
  end

  @impl true
  def init(_args) do
    {:ok, nil}
  end
end
